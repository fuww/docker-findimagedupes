FROM golang:1.17 as build

RUN apt-get update \
  && apt-get install -y libmagic-dev libjpeg-dev libpng-dev libtiff5-dev \
  && rm -rf /var/lib/apt/lists/*

RUN go get -u gitlab.com/opennota/findimagedupes

FROM golang:1.17

RUN apt-get update \
  && apt-get install -y libmagic1 libjpeg62-turbo libpng16-16 libtiff5 \
  && rm -rf /var/lib/apt/lists/*

COPY --from=build /go/bin/findimagedupes /go/bin/findimagedupes

ENTRYPOINT ["/go/bin/findimagedupes"]
